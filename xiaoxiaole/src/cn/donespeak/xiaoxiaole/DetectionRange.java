package cn.donespeak.xiaoxiaole;

public class DetectionRange {
	private int rowTop;
	private int rowBottom;
	private int colLeft;
	private int colRight;
	private int score;

	public DetectionRange() {
		this(0, 0, 0, 0);
	}

	public DetectionRange(int rowTop, int rowBottom, int colLeft, int colRight) {
		this(rowTop, rowBottom, colLeft, colRight, 0);
	}
	
	public DetectionRange(int rowTop, int rowBottom, int colLeft, int colRight, int score) {
		super();
		this.rowTop = rowTop;
		this.rowBottom = rowBottom;
		this.colLeft = colLeft;
		this.colRight = colRight;
		this.score = score;
	}
	
	/**
	 
	 ****
	 **xx&
	   &&&
	   
	   v
	   
	 *****
	 *****
	 *****
	 合并两个矩形，为一个包含两个矩形的最小矩形
	 * @param range1
	 * @param range2
	 * @return
	 */

	public DetectionRange merge(DetectionRange range) {
		rowTop = Math.min(rowTop, range.getRowTop());
		rowBottom = Math.max(rowBottom, range.getRowBottom());
		colLeft = Math.min(colLeft, range.getColLeft());
		colRight = Math.max(colRight, range.getColRight());
		score = score + range.getScore();
		
		return this;
	}

	public static DetectionRange merge(DetectionRange range1, DetectionRange range2) {
		DetectionRange range = new DetectionRange();
		return range.merge(range1).merge(range2);
	}
	
	//** getter and setter **//
	public int getRowTop() {
		return rowTop;
	}
	public void setRowTop(int rowTop) {
		this.rowTop = rowTop;
	}
	public int getRowBottom() {
		return rowBottom;
	}
	public void setRowBottom(int rowBottom) {
		this.rowBottom = rowBottom;
	}
	public int getColLeft() {
		return colLeft;
	}
	public void setColLeft(int colLeft) {
		this.colLeft = colLeft;
	}
	public int getColRight() {
		return colRight;
	}
	public void setColRight(int colRight) {
		this.colRight = colRight;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "DetectionRange [rowTop=" + rowTop + ", rowBottom=" + rowBottom + ", colLeft=" + colLeft + ", colRight="
				+ colRight + ", score=" + score + "]";
	}

	public boolean isInRange(int r, int c) {
		return (rowTop <= r && r <= rowBottom && colLeft <= c && c <= colRight);
	}
}
