package cn.donespeak.xiaoxiaole;

import java.security.InvalidParameterException;
import java.util.Arrays;

/**
 * 以下算法有两个规定
 * 1. 每次交换必须都要存在可以消除的行或者列，否则不能进行交换
 * 2. 任何数值不能和 0（已经被替换的位置）进行交换
 * 
 */
public class XiaoXiaoLe {
	private int rowNum;
	private int colNum;
	private int[][] chessboard;
	private int typeNum;
	private int score;
	private XiaoXiaoLeRemainder xiaoXiaoLeRemainder;
	
	public boolean DEBUG = false;
	public boolean SHOW_REMOVED_MARKS = true;
	public boolean SHOW_PROCESSING = false;
	
	private static final int EMPTY_MARK = 0;
	// 标记该元素将在“行”中被消除
	private static final int ROW_MARK = 1;
	// 标记该元素将在“列”中被消除
	private static final int COL_MARK = 2;
	// 标记该元素将在“行”和“列”中被同时消除
	private static final int BOTH_MARK = ROW_MARK + COL_MARK;
	public static final int MIN_NUM_ELIMINATE = 3;
	
	public static class OperationForbiddenException extends RuntimeException {
		private static final long serialVersionUID = 1L;
		public OperationForbiddenException() {
			super("Operation forbidden");
		}
	}
	public XiaoXiaoLe(int[][] chessboard, int typeNum) {
		this(chessboard, typeNum, 0);
	}
	public XiaoXiaoLe(int[][] chessboard, int typeNum, int score) {
		if(chessboard == null || chessboard.length == 0 || chessboard[0].length == 0 
				|| typeNum <= 0) {
			throw new AssertionError("the row and column num of chessboard shouldn't lower than 1 "
					+ "and typeNum shouldn't be lower than 1");
		}
		this.chessboard = chessboard;
		this.typeNum = typeNum;
		this.rowNum = chessboard.length;
		this.colNum = chessboard[0].length;
		this.score = score;
		
		this.xiaoXiaoLeRemainder = new XiaoXiaoLeRemainder(this);
	}
	
	public XiaoXiaoLe getSnapshot() {
		int[][] newChessboard = new int[rowNum][];
		for(int i = 0; i < rowNum; i ++) {
			newChessboard[i] = Arrays.copyOf(chessboard[i], chessboard[i].length);
		}
		return new XiaoXiaoLe(newChessboard, typeNum, score);
	}

	public int exchangeWithLeft(int row, int col) {
		return exchangeWith(row, col, ArrowE.LEFT);
	}
	
	public int exchangeWithRight(int row, int col) {
		return exchangeWith(row, col, ArrowE.RIGHT);
	}

	public int exchangeWithUp(int row, int col) {
		return exchangeWith(row, col, ArrowE.UP);
	}
	
	public int exchangeWithDown(int row, int col) {
		return exchangeWith(row, col, ArrowE.DOWN);
	}
	
	public int exchangeWith(int row, int col, ArrowE arrow) {
		Point vector = arrow.getVector();
		printlnIfShowAllow("Exchange " + new Point(row, col) + " to " + arrow.getName() + ". ");
		int score = exchange(new Point(row, col), 
				new Point(row + vector.getRow(), col + vector.getCol()));
		if(score > 0) {
			printlnIfShowAllow("Get score: " + score);
		}
		return score;
	}

	private void printlnIfShowAllow(String message) {
		if(SHOW_PROCESSING) {
			System.out.println(message);
		}
	}

	private void printIfShowAllow(String message) {
		if(SHOW_PROCESSING) {
			System.out.print(message);
		}
	}
	
	/**
	 *
	 *
	 * 如果第一个点位置存在可以消除，则将优先被消除调整
	 * 
	 * @param row1 第一个点的行
	 * @param col1 第一个点的列
 	 * @param row2 第二个点的行
 	 * @param col2 第二个点的列
	 */
	private int exchange(Point p1, Point p2) {
		
		checkPointInChessboard(p1);
		checkPointInChessboard(p2);
		checkPointNotZero(p1);
		checkPointNotZero(p2);
		check2PointClose(p1, p2);
		
		swap(p1, p2);
		if(DEBUG) {
			showStatus();
		}
		// 对交换两个点之后的直接结果进行几分和消除
		int increasement = detectAndAdjustFrom2ExchangedPoints(p1, p2);
		// 没有得分，不能进行操作
		if(increasement == 0) {
			// 操作失败恢复原样
			swap(p2, p1);
			throw new OperationForbiddenException();
		}

//		show();
		// 对交换两个点之后导致的棋盘调整进行重新的计算
		increasement += detectAndAdjustAllUntillUnadjustable();
		this.score += increasement;
		return increasement;
	}

	private void checkPointNotZero(Point p) {
		if(isPointZero(p)) {
			throw new OperationForbiddenException();
		}
	}
	
	private boolean isPointZero(Point p) {
		return chessboard[p.getRow()][p.getCol()] == 0;
	}

	/**
	 * 自动检测棋盘中是否存在可以消除的行列
	 * @return
	 */
	public int detectAndAdjustAllUntillUnadjustable() {
		int score = 0;
		while(true) {
			int increasement = detectAndAdjustAllOneTime();
			if(increasement == 0) {
				// 一直自动检测并调整，直到再也没有找到可以自动消除的
				break;
			}
			score += increasement;
		}
		return score;
	}
	
	/**
	 * 计算从点 p 开始，向 arrow 指向的方向和点p点相同的且连续的值的个数
	 * * 1 (1) 1 1 =右=> 2 
	 * @param p 开始的点，不参与到计算结果中
	 * @param arrow 方向
	 * @return 
	 */
	private int countSameValuePointFrom(Point p, ArrowE arrow) {
		int num = 0;
		Point vector = arrow.getVector();
		
		int originalValue = chessboard[p.getRow()][p.getCol()];
		int rowIndex = p.getRow() + vector.getRow();
		int colIndex = p.getCol() + vector.getCol();
		while(isPointInChessboard(rowIndex, colIndex)) {
			if(originalValue != chessboard[rowIndex][colIndex]) {
				break;
			}
			num ++;
			rowIndex += vector.getRow();
			colIndex += vector.getCol();
		}
		return num;
	}
	
	/**
	 * 将第 col 列的，位于(row, col)上方offset距离的点下落位置到从(row, col)开始向上的位置
	 * 这将会从(row, col)开始覆盖
	 * <pre>
	 * 0 0 0  0  0
	 * 0 0 0  1  0
	 * 0 0 0  x  0
	 * 0 0 0  x  0
	 * 0 0 0  *  0
	 * 0 0 0 (*) 0
	 *    v
	 * row = row(*),col=col(*),offset=num(*)
	 *    v
	 * 0 0 0  0  0
	 * 0 0 0  0  0
	 * 0 0 0  0  0
	 * 0 0 0  1  0
	 * 0 0 0  x  0
	 * 0 0 0  x  0
	 * </pre>
	 * @param row 目的位置的行号
	 * @param col 目的位置的列号
	 * @param offset 下降距离
	 */
	private void dropColDownTo(int row, int col, int offset) {
		if(offset == 0) {
			return;
		}
		while(row - offset >= 0) {
			// 如果 chessboard[row][col] == 0，则表示 chessboard[row][col] 上方的所有点都是 0
			chessboard[row][col] = chessboard[row - offset][col];
			row --;
		}
		while(row >= 0 && chessboard[row][col] != 0) {
			chessboard[row][col] = 0;
			row --;
		}
	}
	
	/**
	 * 对整个棋盘进行检查，看是否存在可以消除的行列，
	 * 存在则进行消除，并计算得分然后重新调整
	 * 该检测调整操作仅仅执行一次，即使调整之后仍存在可以消除的行列也不再进行调整
	 */
	private int detectAndAdjustAllOneTime() {
		// 默认所有数值为 0
		int[][] marks = new int[rowNum][colNum];
		// 检测可以消除的行列，并确定最小需要调整范围
		DetectionRange range = new DetectionRange();
		// 棋盘上方都是0，因而可以从下往上检查，以减少对0的点的检查
		for(int row = rowNum - 1; row >= 0; row --) {
			for(int col = 0; col < colNum; col ++) {
				// 对每个点进行一次消除检测
				range.merge(autoDetectAndMarkFrom(new Point(row, col), marks));
			}
		}
		if(range.getScore() > 0) {
			// 同时消除所有需要消除的元素并同时进行下落操作
			eliminateAndDropDownAdjust(range, marks);
		}
		return range.getScore();
	}

	private int detectAndAdjustFrom2ExchangedPoints(Point p1, Point p2) {
		DetectionRange range = new DetectionRange();
		// 默认所有数值为 0
		int[][] marks = new int[rowNum][colNum];
		range.merge(autoDetectAndMarkFrom(p1, marks));
		range.merge(autoDetectAndMarkFrom(p2, marks));
		
		printlnIfDebug("detect from " + p1 + " and " + p2 + ", get range " + range);
		
		eliminateAndDropDownAdjust(range, marks);
		return range.getScore();
	}
	
	private void showMarksIfDebug(int[][] marks) {
		if(!SHOW_PROCESSING || !DEBUG) {
			return;
		}
		for(int[] row: marks) {
			System.out.println(Arrays.toString(row));
		}
	}
	
	/**
	 * 在指定的范围内，消除标记了的元素。消除方法为：从左到右对每一列进行检测，然后对每一列从上到下进行消除和下落。
	 * <pre>
	 *  0  0  0  0
	 * (1)(1)(1) 0
	 *  3  3  2  0
	 *  4 (2)(2)(2)
	 *  
	 *  0  0  0  0
	 * (0)(1)(1) 0
	 *  3  3  2  0
	 *  4 (2)(2)(2)
	 *  
	 *  0  0  0  0
	 * (0)(0)(1) 0
	 *  3  3  2  0
	 *  4 (2)(2)(2)
	 *  
	 *  0  0  0  0
	 * (0)(0)(1) 0
	 *  3  3  2  0
	 *  4 (0)(2)(2)
	 * </pre>
	 * @param range 需要调整的范围
	 * @param marks 标记需要消除的元素
	 */
	private void eliminateAndDropDownAdjust(DetectionRange range, int[][] marks) {
		printlnIfDebug(range.toString());
		printlnIfDebug("=============");
		showMarksIfDebug(marks);
		printlnIfDebug("=============");
		if(SHOW_PROCESSING && SHOW_REMOVED_MARKS) {
			showStatus();
			showRemoving(range, marks);
		}
		for(int c = range.getColLeft(); c <= range.getColRight(); c ++) {
			// 对每一列进行调整，从上到下
			for(int r = range.getRowTop(); r <= range.getRowBottom(); r ++) {
				// 计算被消除的列长度
				int originalR = r;
				while(r <= range.getRowBottom() && marks[r][c] != 0) {
					// 取消标记
					marks[r ++][c] = 0;
				}
				dropColDownTo(r - 1, c, r - originalR);
			}
		}
		showMarksIfDebug(marks);
		printlnIfDebug("=============");
//		show();
	}
	private void showRemoving(DetectionRange range, int[][] marks) {
		System.out.println("\t===== REMOVING =====");
		showRemovedMarks(range, marks);
		System.out.println("\t=========" + range.getScore() + "=========");
		
	}
	private void showRemovedMarks(DetectionRange range, int[][] marks) {
		System.out.print("\t");
		System.out.print("   ");
		for(int i = 0; i < colNum; i ++) {
			System.out.print(" " + i + " ");
		}
		System.out.println();
		System.out.print("\t");
		System.out.print("   ");
		for(int i = 0; i < colNum; i ++) {
			System.out.print(" - ");
		}
		System.out.println();
		for(int r = 0; r < rowNum; r ++) {
			System.out.print("\t");
			System.out.print(r + " |");
			for(int c = 0; c < colNum; c ++) {
				if(range != null && marks != null && marks[r][c] != EMPTY_MARK && range.isInRange(r, c)) {
					System.out.print(" * ");
				} else {
					System.out.print(" " + chessboard[r][c] + " ");
				}
			}
			System.out.println();
		}
	}

	/**
	 * 从点 p，开始向上下左右进行检测，找到可以消除的行列，进行消除标记，然后计算得分和范围
	 * @param p 开始的点
	 * @param marks 消除标记表
	 * @return
	 */
	private DetectionRange autoDetectAndMarkFrom(Point p, int[][] marks) {
		// 注意初始化值，row方向初始化为顶部一条横线，左右颠倒初始化
		DetectionRange range = new DetectionRange(0, 0, this.colNum - 1, 0, 0);
		if(isPointZero(p)) {
			// 0 点不做检查
			return range;
		}
		int row = p.getRow();
		int col = p.getCol();
		int increasement = 0;
		if(marks[row][col] == 0 || marks[row][col] == ROW_MARK) {
			// 该点未被标记为需要消除，或者已经被标记为“行”消除 -》此时还可以被标记为“列”消除
			int numUp = countSameValuePointFrom(p, ArrowE.UP);
			int numDown = countSameValuePointFrom(p, ArrowE.DOWN);
			printlnIfDebug(p + ", numUp: " + numUp + ", numDown: " + numDown);
			int increasementCol = ScoreCalculator.cal(numUp + numDown + 1);
			if(increasementCol > 0) {
				adjustRange(range, 0, row + numDown, col, col);
				for(int r = row - numUp; r <= row + numDown; r ++) {
					marks[r][col] += COL_MARK;
				}
			}
			increasement += increasementCol;
		}
		if(marks[row][col] == 0 || marks[row][col] == COL_MARK) {
			// 该点未被标记为需要消除，或者已经被标记为“列”消除 -》此时还可以被标记为“行”消除
			int numLeft = countSameValuePointFrom(p, ArrowE.LEFT);
			int numRight = countSameValuePointFrom(p, ArrowE.RIGHT);
			printlnIfDebug(p + ", numLeft: " + numLeft + ", numRight: " + numRight);

			int increasementRow = ScoreCalculator.cal(numLeft + numRight + 1);
			if(increasementRow > 0) {
				adjustRange(range, 0, row, col - numLeft, col + numRight);
				for(int c = col - numLeft; c <= col + numRight; c ++) {
					marks[row][c] += ROW_MARK;
				}
			}
			increasement += increasementRow;
		}
		range.setScore(range.getScore() + increasement);
		return range;
	}
	
	private void adjustRange(DetectionRange range, int top, int bottom, int left, int right) {
		// 顶部进行拓展
		range.setRowTop(Math.min(range.getRowTop(), top));
		// 底部进行拓展
		range.setRowBottom(Math.max(range.getRowBottom(), bottom));
		// 左侧进行扩张
		range.setColLeft(Math.min(range.getColLeft(), left));
		// 右侧进行扩张
		range.setColRight(Math.max(range.getColRight(), right));
	}

	void checkPointInChessboard(Point p) {
		if(!isPointInChessboard(p)) {
			throw new OperationForbiddenException();
		}
	}
	
	boolean isPointInChessboard(Point p) {
		return isPointInChessboard(p.getRow(), p.getCol());
	}
	private boolean isPointInChessboard(int row, int col) {
		return (0 <= row && row < this.rowNum) && (0 <= col && col < this.colNum);
	}
	
	private void swap(Point p1, Point p2) {
		swap(p1.getRow(), p1.getCol(), p2.getRow(), p2.getCol());
	}
	private void swap(int row1, int col1, int row2, int col2) {
		int temp = chessboard[row1][col1];
		chessboard[row1][col1] = chessboard[row2][col2];
		chessboard[row2][col2] = temp;
	}
	
	public boolean canContinue() {
		return xiaoXiaoLeRemainder.getTips(1).size() > 0;
	}

	private void check2PointClose(Point p1, Point p2) {
		if(!is2PointClose(p1, p2)) {
			throw new OperationForbiddenException();
		}
	}
	
	private boolean is2PointClose(Point p1, Point p2) {
		if(p1.getRow() == p2.getRow()) {
			return Math.abs(p1.getCol() - p2.getCol()) == 1;
		} else {
			return (p1.getCol() == p2.getCol()) && (Math.abs(p1.getRow() - p2.getRow()) == 1);
		}
	}
	
	private void printIfDebug(String message) {
		if(DEBUG) {
			System.out.print(message);
		}
	}
	private void printlnIfDebug(String message) {
		if(DEBUG) {
			System.out.println(message);
		}
	}
	
	public void show() {
		showStatus();
		System.out.println("Your score:\t" + score);
	}
	
	public void showStatus() {
		System.out.println("\t====== STATUS ======");
		showRemovedMarks(null, null);
		System.out.println("\t====================");
	}
	
	//** getter and setter **//
	public int getRowNum() {
		return rowNum;
	}
	public int getColNum() {
		return colNum;
	}
	public int getTypeNum() {
		return typeNum;
	}
	public int getScore() {
		return score;
	}
	public int getChessboardCell(Point p) {
		return getChessboardCell(p.getRow(), p.getCol());
	}
	public int getChessboardCell(int row, int col) {
		return chessboard[row][col];
	}
	public XiaoXiaoLeRemainder getRemainder() {
		return xiaoXiaoLeRemainder;
	}
	public int getMinNumEliminate() {
		return MIN_NUM_ELIMINATE;
	}
}
