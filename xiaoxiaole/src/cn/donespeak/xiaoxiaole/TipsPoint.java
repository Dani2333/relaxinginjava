package cn.donespeak.xiaoxiaole;

public class TipsPoint extends Point {
	// 可以考虑加入方向 ArrowE
	private ArrowE arrow;

	public TipsPoint(int row, int col, ArrowE arrow) {
		super(row, col);
		this.arrow = arrow;
	}
	
	public TipsPoint() {
		super();
	}
	
	public ArrowE getArrow() {
		return arrow;
	}
	public void setArrow(ArrowE arrow) {
		this.arrow = arrow;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((arrow == null) ? 0 : arrow.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipsPoint other = (TipsPoint) obj;
		if (arrow != other.arrow)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + " move " + arrow.getArrow();
	}
}
