package cn.donespeak.xiaoxiaole;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cn.donespeak.xiaoxiaole.XiaoXiaoLe.OperationForbiddenException;

public class AutoPlayer {
	private XiaoXiaoLe xiaoXiaoLe;
	private int stepsLeft;
	private List<TipsPoint> tips;
	private int maxScore;
	
	public AutoPlayer(XiaoXiaoLe xiaoXiaoLe, int stepsLeft) {
		this.xiaoXiaoLe = xiaoXiaoLe;
		this.stepsLeft = stepsLeft;
		this.tips = new LinkedList<TipsPoint>();
		this.maxScore = 0;
	}
	
	public void start() {
		maxScore = getMaxScore(xiaoXiaoLe, stepsLeft, tips);
	}
	
	public int getMaxScore() {
		return maxScore;
	}
	
	public List<TipsPoint> getTips() {
		return tips;
	}
	
	private int getMaxScore(XiaoXiaoLe xiaoXiaoLe, int stepsLeft, List<TipsPoint> tips) {
		if(stepsLeft == 0) {
			return 0;
		}
		int maxScore = 0;
		List<TipsPoint> tipsPicked = new ArrayList<>();
		for(int r = xiaoXiaoLe.getRowNum() - 1; r >= 0; r --) {
			for(int c = 0; c < xiaoXiaoLe.getColNum(); c ++) {

				if(xiaoXiaoLe.getChessboardCell(r, c) == 0) {
					continue;
				}
				List<TipsPoint> tipsRight = new ArrayList<>();
				List<TipsPoint> tipsUp = new ArrayList<>();
				XiaoXiaoLe xiaoXiaoLeRight = xiaoXiaoLe.getSnapshot();
				XiaoXiaoLe xiaoXiaoLeUp = xiaoXiaoLe.getSnapshot();
				
				int scoreRight = exchangeRight(xiaoXiaoLeRight, r, c, stepsLeft, tipsRight);
				int scoreUp = exchangeUp(xiaoXiaoLeUp, r, c, stepsLeft, tipsUp);
				
				int maxIncreasement = Math.max(scoreRight, scoreUp);
				
				maxScore = Math.max(maxIncreasement, maxScore);

				if(maxIncreasement == maxScore) {
					if(maxIncreasement == scoreRight) {
						if(scoreRight == scoreUp) {
							// 两个方法得分相同时候，取路径最短的
							tipsPicked = tipsRight.size() < tipsUp.size()? tipsRight: tipsUp;
						} else {
							tipsPicked = tipsRight;
						}
					} else if(maxIncreasement == scoreUp) {
						tipsPicked = tipsUp;
					}
				}
			}
		}
		tips.addAll(tipsPicked);
		return maxScore;
	}

	private int exchangeUp(XiaoXiaoLe xiaoXiaoLe, int r, int c, int stepsLeft, List<TipsPoint> tips) {
		return exchange(xiaoXiaoLe, r, c, stepsLeft, ArrowE.UP, tips);
	}

	private int exchangeRight(XiaoXiaoLe xiaoXiaoLe, int r, int c, int stepsLeft, List<TipsPoint> tips) {
		return exchange(xiaoXiaoLe, r, c, stepsLeft, ArrowE.RIGHT, tips);
	}
	
	private int exchange(XiaoXiaoLe xiaoXiaoLe, int r, int c, int stepsLeft, ArrowE arrow, 
			List<TipsPoint> tips) {
		int increasement = 0;
		try {
			tips.add(new TipsPoint(r, c, arrow));
			increasement += xiaoXiaoLe.exchangeWith(r, c, arrow);
			increasement += getMaxScore(xiaoXiaoLe, stepsLeft - 1, tips);
			// 如果操作允许，也就是没有抛出异常，其该操作得到分数，则添加该操作
		} catch (OperationForbiddenException e) {
			// do nothing
		} finally {
			if(increasement == 0) {
				tips.remove(tips.size() - 1);
			}
		}
		return increasement;
	}
}
