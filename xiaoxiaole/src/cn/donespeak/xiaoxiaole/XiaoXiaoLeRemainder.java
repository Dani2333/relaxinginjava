package cn.donespeak.xiaoxiaole;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class XiaoXiaoLeRemainder {
	
	private XiaoXiaoLe xiaoXiaoLe;
	private int rowNum;
	private int colNum;
	private final int MIN_NUM_ELIMINATE;
	
	private static final int EMPTY_MARK = 0;
	
	XiaoXiaoLeRemainder(XiaoXiaoLe xiaoXiaoLe) {
		this.xiaoXiaoLe = xiaoXiaoLe;
		this.rowNum = xiaoXiaoLe.getRowNum();
		this.colNum = xiaoXiaoLe.getColNum();
		this.MIN_NUM_ELIMINATE = xiaoXiaoLe.getMinNumEliminate();
	}

	public void showTips() {
		showTips(-1);
	}
	
	public void showTips(int num) {
		List<Point> tips = getTips();
		boolean[][] marks = new boolean[rowNum][colNum];
		for(Point p: tips) {
			marks[p.getRow()][p.getCol()] = true;
		}
		System.out.println("Tips:");
		for(int r = 0; r < rowNum; r ++) {
			System.out.print("\t");
			for(int c = 0; c < colNum; c ++) {
				if(marks[r][c]) {
					System.out.print("(" + xiaoXiaoLe.getChessboardCell(r, c) + ")");
				} else {
					System.out.print(" " + xiaoXiaoLe.getChessboardCell(r, c) + " ");
				}
			}
			System.out.println();
		}
	}

	/**
	 * 
	 * @param num 获取指定个数提示，num < 0 表示全部，当总个数小于 num 时，返回总个数个
	 * @return
	 */
	public List<Point> getTips(int num) {
		if(num == 0) {
			return new ArrayList<>();
		}
		Set<Point> tipPoints = new HashSet<Point>();
		// 检查“行”是否存在两个且两端存在交换即可变成3个的点
		getRowTips(tipPoints, num);
		// 检查“列”是否存在两个且两端存在交换即可变成3个的点
		getColTips(tipPoints, num);
		return new ArrayList<>(tipPoints);
	}
	
	public void getRowTips(Set<Point> tipPoints, int num) {
		if(num > 0 && tipPoints.size() >= num) {
			return;
		}
		for(int row = 0; row < rowNum; row ++) {
			for(int col = 0; col <= colNum - MIN_NUM_ELIMINATE + 1; col ++) {
				int curValue = xiaoXiaoLe.getChessboardCell(row, col);
				if(curValue == EMPTY_MARK) {
					continue;
				}
				boolean needOneToEliminate = true;
				for(int i = 1; i < MIN_NUM_ELIMINATE - 1; i ++) {
					if(curValue != xiaoXiaoLe.getChessboardCell(row, col + i)) {
						needOneToEliminate = false;
						break;
					}
				}
				if(needOneToEliminate) {
					// 检查左侧三个点
					//   *
					// * x (1) 1
					//   *
					checkAndAddTipsPoints(curValue, row, col - 1, Arrays.asList(ArrowE.UP, ArrowE.LEFT, ArrowE.DOWN),
							tipPoints, num);
					// 检查右侧侧三个点
					//       *
					// (1) 1 x *
					//       *
					checkAndAddTipsPoints(curValue, row, col + MIN_NUM_ELIMINATE - 1, 
						Arrays.asList(ArrowE.UP, ArrowE.RIGHT, ArrowE.DOWN), tipPoints, num);
					
					if(num > 0 && tipPoints.size() >= num) {
						return;
					}
				}
			}
		}
	}
	
	public void getColTips(Set<Point> tipsPoints, int num) {
		if(num > 0 && tipsPoints.size() >= num) {
			return;
		}
		for(int col = 0; col < colNum; col ++) {
			for(int row = 0; row <= rowNum - MIN_NUM_ELIMINATE + 1; row ++) {
				boolean needOneToEliminate = true;
				int curValue = xiaoXiaoLe.getChessboardCell(row, col);
				if(curValue == EMPTY_MARK) {
					continue;
				}
				for(int i = 1; i < MIN_NUM_ELIMINATE - 1; i ++) {
					if(curValue != xiaoXiaoLe.getChessboardCell(row + i, col)) {
						needOneToEliminate = false;
					}
				}
				if(needOneToEliminate) {
					// 检查上方三个点
					//   *
					// * x *
					//  (1)
					//   1
					checkAndAddTipsPoints(curValue, row - 1, col, Arrays.asList(ArrowE.LEFT, ArrowE.UP, ArrowE.RIGHT), 
							tipsPoints, num);
					// 检查下方三个点
					//  (1)
					//   1
					// * x *
					//   *
					checkAndAddTipsPoints(curValue, row + MIN_NUM_ELIMINATE - 1, col, 
							Arrays.asList(ArrowE.LEFT, ArrowE.DOWN, ArrowE.RIGHT), tipsPoints, num);
					if(num > 0 && tipsPoints.size() >= num) {
						return;
					}
				}
			}
		}
	}
	
	private void checkAndAddTipsPoints(int curValue, int row, int col, List<ArrowE> arrows, 
			Collection<Point> tipsPoints, int limitedNum) {
		if(!xiaoXiaoLe.isPointInChessboard(new Point(row, col))
				|| (limitedNum >= 0 && tipsPoints.size() >= limitedNum)) {
			return;
		}
		boolean tipsExists = false;
		for(ArrowE arrow: arrows) {
			Point vector = arrow.getVector();
			int rowIndex = vector.getRow() + row;
			int colIndex = vector.getCol() + col;
			if(xiaoXiaoLe.isPointInChessboard(new Point(rowIndex, colIndex)) 
					&& xiaoXiaoLe.getChessboardCell(rowIndex, colIndex) == curValue) {
				if(limitedNum > 0 && tipsPoints.size() >= limitedNum) {
					return;
				}
//				System.out.println(new Point(row, col) + " " + arrow.getArrow() + " " + new Point(rowIndex, colIndex) + ")");
				tipsPoints.add(new Point(rowIndex, colIndex));
				tipsExists = true;
			}
		}
		if(tipsExists) {
			tipsPoints.add(new Point(row, col));
		}
	}
	
	public List<Point> getTips() {
		return getTips(-1);
	}
}
