package cn.donespeak.xiaoxiaole;

import java.util.HashMap;
import java.util.Map;

public enum ArrowE {
	UP(1, "^", "up", new Point(-1, 0)),
	RIGHT(2, ">", "right", new Point(0, 1)),
	DOWN(3, "v", "down", new Point(1, 0)),
	LEFT(4, "<", "left", new Point(0, -1))
	;
	
	private final int value;
	private final String arrow;
	private final String name;
	private final Point vector;
	
	private static final Map<ArrowE, ArrowE> arrowMapReverse;
	
	static {
		arrowMapReverse = new HashMap<ArrowE, ArrowE>();
		addReverse(ArrowE.UP, ArrowE.DOWN);
		addReverse(ArrowE.LEFT, ArrowE.RIGHT);
	}
	
	private static void addReverse(ArrowE arrow, ArrowE reverse) {
		arrowMapReverse.put(arrow, reverse);
		arrowMapReverse.put(reverse, arrow);
	}
	
	private ArrowE(int value, String arrow, String name, Point vector) {
		this.value = value;
		this.arrow = arrow;
		this.name = name;
		this.vector = vector;
	}
	
	public ArrowE getReverse() {
		return arrowMapReverse.get(this);
	}
	public int getValue() {
		return value;
	}
	public String getArrow() {
		return arrow;
	}
	public String getName() {
		return name;
	}
	public Point getVector() {
		return vector;
	}
	
	public static ArrowE getE(int value) {
		for(ArrowE arrow: values()) {
			if(arrow.getValue() == value) {
				return arrow;
			}
		}
		return null;
	}
}
